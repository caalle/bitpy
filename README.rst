#####
bitpy
#####

Python bindings for Bitbucket's rest API.

*************
Documentation
*************

Full documentation, including installation and setup guides, can be found at
http://bitpy.readthedocs.org/en/latest/

*************
Bugs & issues
*************

Any feature requests or issues can be added to 
https://bitbucket.org/caalle/bitpy/issues

*********************
Copyright and license
*********************

Copyright (c) 2014 Carl Claesson

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this work except in compliance with the License. You may obtain a copy of the
License in the LICENSE.txt file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
