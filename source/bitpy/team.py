# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson

from . import (
    entity, exception, member
)


class Team(entity.Entity):
    '''Represent a team.'''

    def __init__(self, client, **kwargs):
        '''Initialise new team with *client*.'''
        self._name = kwargs['username']
        self._client = client

        if not kwargs:
            kwargs = dict()

        for key in kwargs.iterkeys():
            setattr(self, '_{0}'.format(key), kwargs[key])

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}({1})>'.format(
            self.__class__.__name__, self._username
        )

    def _request(
        self, method, resource=None, resource_id=None, payload=None,
        appendix=None
    ):
        '''Base request for repository.'''

        if not self._client:
            raise exception.NoAttachedClientError(
                '{class_name} is missing attached BitbucketClient'.format(
                    class_name=self.__class__.__name__
                )
            )

        return self._client._request(
            method, 'teams', account=self._name, resource=resource,
            resource_id=resource_id, payload=payload, appendix=appendix
        )

    def get_members(self):
        '''Return list of :py:class:`bitpy.member.Member` objects.'''
        values = self._request('GET', appendix='members').json()['values']

        return [
            member.Member(self._client, username=data['username'], data=data)
            for data in values
        ]
