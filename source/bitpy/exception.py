# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson


class StandardError(Exception):
    pass


class BitbucketPermissionsError(StandardError):
    pass


class NoAttachedClientError(StandardError):
    pass
