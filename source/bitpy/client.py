# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson

import json
import logging

import requests
from requests.auth import HTTPBasicAuth

from . import exception, repository, team, result_set, member


class Client(object):
    '''Bitbucket REST API used for communication with Bitbucket.'''

    base_url = 'https://bitbucket.org/api/{version}'

    def __init__(self, username, password, version=2.0):
        '''Initialise Bitbucket rest api with *username* and *password*.'''
        self._username = username
        self._password = password

        self.url = self.base_url.format(version=version)

        self.logger = logging.getLogger(self.__class__.__name__)

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}>'.format(
            self.__class__.__name__,
        )

    def _request(
        self, method, base_resource, base_resource_id=None, account=None,
        resource=None, resource_id=None, payload=None, appendix=None
    ):
        '''Base method for requests.'''
        if not account:
            account = self._username

        full_url = '{url}/{base_resource}/{account}'.format(
            url=self.url, base_resource=base_resource, account=account
        )

        if base_resource_id:
            full_url = '{base_url}/{base_resource_id}'.format(
                base_url=full_url, base_resource_id=base_resource_id
            )

        if resource:
            full_url = '{base_url}/{resource}'.format(
                base_url=full_url, resource=resource
            )

        if resource_id:
            full_url = '{base_url}/{resource_id}'.format(
                base_url=full_url, resource_id=resource_id
            )

        if appendix:
            full_url = '{base_url}/{appendix}'.format(
                base_url=full_url, appendix=appendix
            )

        return self._call(method, full_url, payload)

    def _call(self, method, url, payload=None):
        '''Basic request to Bitbucket using *method* and *url*.

        Optional *payload* can be specified.

        '''
        if not payload:
            payload = dict()

        self.logger.info(url)

        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        response = requests.request(
            method, url, auth=HTTPBasicAuth(self._username, self._password),
            data=json.dumps(payload), headers=headers
        )

        if response.status_code == 403:
            raise exception.BitbucketPermissionError(
                'Not authorized to get resource.'
            )

        if response.status_code not in (200, 201):
            try:
                errorData = response.json()['error']
                errorFields = errorData.get('fields', {})
                raise exception.StandardError(
                    '{0}:: {1}'.format(
                        errorData['message'],
                        ', '.join(
                            [
                                '{0}:{1}'.format(
                                    key, errorFields[key].pop()
                                ) for key in errorFields.iterkeys()
                            ]
                        )
                    )
                )
            except KeyError:
                raise exception.StandardError()

        return response

    def get_repository(self, name, account=None):
        '''Return :py:class:`bitpy.repository.Repository` with *name*.

        If no *account* is specified the currently logged in user will
        be used.

        '''
        return repository.Repository(
            self, **self._request(
                'GET', 'repositories', name, account
            ).json()
        )

    def list_repositories(self, account=None):
        '''Return list of :py:class:`bitpy.repository.Repository`.

        If no *account* is specified all public repositores on
        Bitbucket will be returned.

        '''
        return result_set.RepositoryListResultSet(self, account)

    def get_team(self, name):
        '''Return :py:class:`bitpy.team.Team` with *name*.'''
        return team.Team(self, username='ftrack')

    def get_member(self, username):
        '''Return :py:class:`bitpy.member.Member` with *username*.'''
        return member.Member(
            self, **self._request(
                'GET', 'users', account=username
            ).json()
        )
