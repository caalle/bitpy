# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson


class ListResultSet(object):
    '''List result set.'''

    def __init__(self, client, return_class, first_response):
        '''Initialse result set.'''
        self._client = client
        self._return_class = return_class
        self._first_response = first_response

    def __iter__(self):
        '''Return generator when iterating.'''
        return self._generator()

    def _generator(self):
        '''Generator for yielding result.'''
        has_next_page = True
        next_page = False
        response = self._first_response

        while has_next_page:

            if next_page:
                response = self._client._call('GET', next_page).json()

            try:
                next_page = response['next']
            except KeyError:
                has_next_page = False

            for value in response['values']:
                yield self._return_class(
                    client=self._client, **value
                )


class RepositoryListResultSet(ListResultSet):
    '''List result for repositories.'''

    def __init__(self, client, account=None):
        '''Initialise list result with a bitpy *client*.

        Optional *account* can be specified to only return repositories
        for a certain Bitbucket account. If *account* is `None` all public
        repositories on Bitbucket will be returned.

        '''
        url = '{url}/{base_resource}'.format(
            url=client.url, base_resource='repositories'
        )

        if account:
            url = '{url}/{account}'.format(
                url=url, account=account
            )

        response = client._call(
            'GET', url
        ).json()

        # Inline to avoid circular import.
        from . import repository

        super(RepositoryListResultSet, self).__init__(
            client, repository.Repository, response
        )


class PullRequestListResultSet(ListResultSet):
    '''List result for pull requests.'''

    def __init__(self, client, repository):
        '''Initialise list result with a bitpy *client* and *repository*.'''
        response = client._call(
            'GET', repository._links['pullrequests']['href']
        ).json()

        # Inline to avoid circular import.
        from . import pull_request

        super(PullRequestListResultSet, self).__init__(
            client, pull_request.PullRequest, response
        )
