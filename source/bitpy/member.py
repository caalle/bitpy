# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson

from . import entity, result_set


class Member(entity.Entity):
    '''Represent a Bitbucket member.'''

    def __init__(self, client, **kwargs):
        '''Initialise new member with *username*.

        A dictionary *data* can be specified and will be stored
        as _private attributes.

        '''
        self._client = client
        self._username = kwargs['username']

        if not kwargs:
            kwargs = dict()

        if 'data' in kwargs:
            for key in kwargs['data'].iterkeys():
                setattr(self, '_{0}'.format(key), kwargs['data'][key])

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}({1})>'.format(
            self.__class__.__name__, self._username
        )

    def get_username(self):
        '''Return username of member.'''
        return self._username

    def get_display_name(self):
        '''Return the fullname of the member.'''
        return self._display_name

    def get_followers(self):
        '''Return followers for the member.

        Will return a list of :py:class:`bitpy.member.Member`.

        '''
        response = self._client._call(
            'GET', self._links['followers']['href']
        ).json()
        return [
            Member(self._client, **data) for data in response['values']
        ]

    def get_following(self):
        '''Return members and teams the member is following.

        Will return a mixed list of :py:class:`bitpy.member.Member` and
        :py:class:`bitpy.team.Team`.

        '''
        response = self._client._call(
            'GET', self._links['following']['href']
        ).json()

        # Inline to avoid circular import.
        from . import team

        following = []
        for data in response['values']:

            if data['type'] == 'team':
                following.append(
                    team.Team(self._client, **data)
                )
            else:
                following.append(
                    Member(self._client, **data)
                )

        return following

    def get_repositories(self):
        '''Return list of repositories belonging to member.'''
        return result_set.RepositoryListResultSet(
            self._client, account=self._username
        )
