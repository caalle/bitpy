# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson

from . import (
    entity, pull_request, exception, result_set
)


class Repository(entity.Entity):

    def __init__(self, client, **kwargs):
        '''Initialise repository with *client*.

        Requires name of the repository as a key-value argument *name*.

        *client* should be of type `bitbucket.client.Client` and are used
        for all communication with Bitbucket. Any *kwargs* will be stored
        as private attributes.

        '''
        self._client = client

        if not 'name' in kwargs:
            raise exception.StandardError(
                'Repository cannot be initialised without "name"'
            )

        if not kwargs:
            kwargs = dict()

        for key in kwargs.iterkeys():
            setattr(self, '_{0}'.format(key), kwargs[key])

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}({1})>'.format(
            self.__class__.__name__, self._name
        )

    def _request(
        self, method, resource, resource_id=None, payload=None, appendix=None
    ):
        '''Base request for repository.'''

        if not self._client:
            raise exception.NoAttachedClientError(
                '{class_name} is missing attached BitbucketClient'.format(
                    class_name=self.__class__.__name__
                )
            )

        return self._client._request(
            method, 'repositories', self._name, self._owner['username'],
            resource, resource_id=resource_id,
            payload=payload, appendix=appendix
        )

    def list_pull_requests(self):
        '''Return list of :py:class:`bitpy.pull_request.PullRequest` items.'''
        return result_set.PullRequestListResultSet(self._client, self)

    def get_pull_request(self, id):
        '''Return PullRequest with *id*.'''
        response = self._request('GET', 'pullrequests', id)

        data = response.json()
        return pull_request.PullRequest(
            self._client, **data
        )

    def create_pull_request(
        self, title, description, source_branch, target_branch,
        close_source_branch=False, reviewers=None
    ):
        '''Create a new :py:class:`bitpy.pull_request.PullRequest`.'''

        payload = {
            'title': title,
            'description': description,
            'source': {
                'branch': {
                    'name': source_branch
                }
            },
            'destination': {
                'branch': {
                    'name': target_branch
                }
            },
            'close_source_branch': close_source_branch,
            'reviewers': reviewers
        }

        response = self._request(
            'POST', 'pullrequests', payload=payload
        ).json()

        return pull_request.PullRequest(
            self._client, **response
        )
