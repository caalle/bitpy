# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson


class Entity(object):
    '''Base class for bitbucket resources.'''

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}>'.format(
            self.__class__.__name__,
        )
