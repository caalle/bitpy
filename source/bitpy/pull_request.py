# :coding: utf-8
# :copyright: Copyright (c) 2014 Carl Claesson

from . import entity


class PullRequest(entity.Entity):
    '''Represent a pull request.'''

    def __init__(self, client, **kwargs):
        '''Initialise new pull request with *client*.'''
        self._client = client

        if not kwargs:
            kwargs = dict()

        for key in kwargs.iterkeys():
            setattr(self, '_{0}'.format(key), kwargs[key])

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0}({1})>'.format(
            self.__class__.__name__, self._id
        )

    def approve(self):
        '''Approve pull request.'''
        self._client._call(
            'POST', self._links['approve']['href']
        )

    def unapprove(self):
        '''Unapprove pull request.'''
        self._client._call(
            'DELETE', self._links['approve']['href']
        )

    def decline(self, reason=None):
        '''Decline pull request.

        Optional *reason* for decline can be specified.

        '''

        payload = {
            'message': reason
        }

        self._client._call(
            'POST', self._links['decline']['href'], payload=payload
        )

    def merge(self, message=None, close_source_branch=None):
        '''Merge pull request.

        Optional commit *message* can be specifed or a default will be
        generated. To override default pull request behaviour for closing
        source branch specify *close_source_branch*.

        '''

        if not message:
            message = 'Merged in {0} (pull request #{1})'.format(
                self.get_destination(), self._id
            )

        payload = {
            'message': message
        }

        if not close_source_branch is None:
            payload['close_source_branch'] = close_source_branch

        self._client._call(
            'POST', self._links['merge']['href'], payload=payload
        )

    def get_link(self):
        '''Return http link to pull request.'''
        return self._links['html']['href']

    def get_destination(self, full_path=False):
        '''Return name of destination branch.

        Set *full_path* to include the owner and repository
        name in destination.

        '''
        destination = self._destination['branch']['name']

        if full_path:
            destination = '{root}/{destination}'.format(
                root=self._destination['repository']['full_name'],
                destination=destination
            )

        return destination

    def get_participants(self):
        '''Return list of pull request participants as dictionaries.

        The dictionaries are formatted as:

        .. code-block:: python

            {
                'role': 'REVIEWER' or 'PARTICIPANT',
                'username': 'caalle',
                'approved': True or False
            }

        '''
        participants = []
        for participant in self._participants:
            participants.append({
                'role': participant['role'],
                'approved': participant['approved'],
                'username': participant['user']['username'],
                'display_name': participant['user']['display_name'],
                'icon': participant['user']['links']['avatar']['href']
            })

        return participants
