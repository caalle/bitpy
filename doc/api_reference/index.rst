..
    :copyright: Copyright (c) 2014 Carl Claesson

.. _api_reference:

*************
API reference
*************

:mod:`bitpy`
======================================

.. automodule:: bitpy.client

.. automodule:: bitpy.repository

.. automodule:: bitpy.pull_request

.. automodule:: bitpy.team

.. automodule:: bitpy.member