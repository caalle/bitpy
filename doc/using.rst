..
    :copyright: Copyright (c) 2014 Carl Claesson

.. _using:

*****
Using
*****

These examples assumes that you have already downloaded and installed bitpy.

Getting a repository
====================

First you need to create a new bitpy client. To do this bitpy requires your
Bitbucket username and password.

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

Once you've got a client you can use that to retrive repositories. The example
below will get the bitpy repository.

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

Manage pull requests
====================

When you've retrived a repository you can both list and create pull requests. To
list all open pull requests for the repository do something like:

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

    for pull_request in bitpy_repository.list_pull_requests():
        print(pull_request._title)

To approve and merge a specific pull request pass in the id as argument.

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

    pull_request = bitpy_repository.get_pull_request(pull_request_id=123)

    pull_request.approve()
    pull_request.merge()

You can also create new pull requests by using the `create_pull_request` method.

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

    pull_request = bitpy_repository.create_pull_request(
        title='A title of the pull request',
        description='''Longer description that might include *markdown*''',
        source_branch='feature/branch', target_branch='master'
    )

    print(pull_request.get_link()) # Output http link to pull request.

Teams and Members
=================

When creating pull requests you might want to add people as reviewers. Let's create
a pull request and add the user 'caalle' as reviewer.

.. code-block:: python

    import bitpy

    client = bitpy.Client(<username>, <password>)

    member = client.get_member('caalle')

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

    pull_request = bitpy_repository.create_pull_request(
        title='Pull request that needs to be reviewed.',
        description='''Longer description that might include *markdown*''',
        source_branch='feature/branch', target_branch='master',
        reviewers=[member]
    )

.. note::

    In a real world scenario the user who creates the pull request cannot be added
    as a reviewer.

If you have a team on Bitbucket and want to use it when assigning to pull requests you can
do something like this:
    
.. code-block:: python
    
    import random
    import bitpy

    client = bitpy.Client(<username>, <password>)

    team = client.get_team('my_bitbucket_team')
    team_members = team.get_members()

    reviewer = [random.choice(team_members)]

    bitpy_repository = client.get_repository(name='bitpy', account='caalle')

    pull_request = bitpy_repository.create_pull_request(
        title='Pull request with random reviewer.',
        description='''Longer description that might include *markdown*''',
        source_branch='feature/branch', target_branch='master',
        reviewers=[reviewer]
    )

This will assign a random user from the team as reviewer for my newly created pull request.
